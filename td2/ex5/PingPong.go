package main

import (
	"fmt"
	"time"
)

type Agent interface {
	Start()
}

type PingAgent struct {
	ID string
	c  chan string
}

func (pa *PingAgent) Start() {
	for {
		pa.c <- "ping"
		fmt.Println(pa.ID, ": ping")
		time.Sleep(1 * time.Second)
	}
}

type PongAgent struct {
	ID string
	c  chan string
}

func (pa *PongAgent) Start() {
	for {
		message := <-pa.c
		if message == "ping" {
			fmt.Println(pa.ID, ": pong")
		}
	}
}

func main() {
	channel := make(chan string)

	pongAgent := &PongAgent{
		ID: "PongAgent",
		c:  channel,
	}
	go pongAgent.Start()

	// Let's say we want 3 PingAgents
	for i := 1; i <= 3; i++ {
		pingAgent := &PingAgent{
			ID: fmt.Sprintf("PingAgent%d", i),
			c:  channel,
		}
		go pingAgent.Start()
		time.Sleep(500 * time.Millisecond) // small delay to stagger the PingAgent starts
	}

	// Let them ping pong for a bit (e.g., 10 seconds), then exit
	time.Sleep(10 * time.Second)
}
