package main

import (
	"fmt"
	"sync"
)

type Synchronizedint struct {
	sync.Mutex
	val int
}

var n Synchronizedint

func f() {
	n.Mutex.Lock()
	n.val++
	n.Mutex.Unlock()
}

func main() {
	for i := 0; i < 10000; i++ {
		go f()
	}

	fmt.Println("Appuyez sur entrée")
	fmt.Scanln()
	fmt.Println("n:", n.val)
}
