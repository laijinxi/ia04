package main

import (
	"fmt"
	"sync"
	"time"
)

// remplit tab avec la valeur v
func FillN(tab []int, v int) {
	for i := range tab {
		tab[i] = v
	}
}

func FillP(tab []int, v int) {
	var wg sync.WaitGroup
	//nb de Goroutine := 4
	nbGoroutine := 4
	chunksize := len(tab) / nbGoroutine

	for i := 0; i < nbGoroutine; i++ {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			start := idx * chunksize
			end := start + chunksize
			//u should check the rest len(tab)%4 case of array
			if idx == (nbGoroutine - 1) {
				end = len(tab)
			}
			for j := start; j < end; j++ {
				tab[j] = v
			}
		}(i)
	}
	wg.Wait()
}

func add1(n int) int {
	n++
	return n
}

// applique f sur chaque élément de tab et remplace la valeur initiale par le résultat de f
func ForEachN(tab []int, f func(int) int) {
	for i := range tab {
		tab[i] = f(tab[i])
	}
}

func ForEachP(tab []int, f func(int) int) {
	var wg sync.WaitGroup
	chunksize := len(tab) / 4
	for i := 0; i < 4; i++ {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			start := idx * chunksize
			end := (idx + 1) * chunksize
			for j := start; i < end; i++ {
				tab[j] = f(tab[j])
			}
		}(i)
	}
	wg.Wait()
}

// copy normal le tableau src dans dest
func CopyN(src []int, dest []int) {
	for i, v := range src {
		dest[i] = v
	}
}

func CopyP(src []int, dest []int) {
	var wg sync.WaitGroup
	chunksize := len(src) / 4

	for i := 0; i < 4; i++ {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			start := idx * chunksize
			end := start + chunksize

			for j := start; j < end; j++ {
				dest[j] = src[j]
			}
		}(i)
	}
	wg.Wait()
}

// vérifie que tab1 et tab2 sont identiques
func EqualN(tab1 []int, tab2 []int) bool {
	for i := 0; i < len(tab1); i++ {
		if tab1[i] != tab2[i] {
			return false
		}
	}
	return true
}

// vérifie que tab1 et tab2 sont identiques
func EqualP(tab1 []int, tab2 []int) bool {
	if len(tab1) != len(tab2) {
		return false
	}
	var wg sync.WaitGroup
	var once sync.Once
	nbGoroutine := 4
	chunksize := len(tab1) / nbGoroutine

	done := make(chan bool)
	result := make(chan bool, 1)

	for i := 0; i < nbGoroutine; i++ {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			start := idx * chunksize
			end := start + chunksize
			if idx == nbGoroutine-1 {
				end = len(tab1)
			}

			for j := start; j < end; j++ {
				select {
				case <-done:
					return
				default:
					if tab1[j] != tab2[j] {
						once.Do(func() {
							close(done)
							result <- false
						})
					}
				}
			}

			/*for j := start; j < end; j++ {
				if tab1[j] != tab2[j] {
					switch {
					case result <- :

					default:

					}
				return
				}
			} */
		}(i)
	}
	wg.Wait()
	res, ok := <-result
	if ok && !res {
		return false
	}
	return true
}

// trouve la valeur et le premier élément de tab vérifiant f (-1, 0 si rien n'est trouvé)
func FindN(tab []int, f func(int) bool) (index int, val int) {
	for i, v := range tab {
		if f(v) == true {
			return i, v
		}
	}
	return -1, 0
}

// trouve la valeur et le premier élément de tab vérifiant f (-1, 0 si rien n'est trouvé)
func FindP(tab []int, f func(int) bool) (index int, val int) {
	var wg sync.WaitGroup
	nbGoroutine := 4
	chunksize := len(tab) / nbGoroutine
	done := make(chan bool)
	res := make(chan [2]int, 1)

	for i := 0; i < nbGoroutine; i++ {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			start := idx * chunksize
			end := start + chunksize
			if idx == nbGoroutine-1 {
				end = len(tab)
			}

			for j := start; j < end; j++ {
				switch {
				case <-done:
					return
				default:
					if f(tab[j]) {
						res <- [2]int{j, tab[j]}
						close(done)
					}
				}

			}
		}(i)
	}
	wg.Wait()
	tmpSlice, ok := <-res
	if ok {
		return tmpSlice[0], tmpSlice[1]
	}
	return -1, 0

}

// crée un nouveau tableau à partit de tab
// tel que les éléments du nouveau tableau sont composés des éléments du précédent tableau auxquels on a appliqué la fonction f
func MapN(tab []int, f func(int) int) []int {
	for i, v := range tab {
		tab[i] = f(v)
	}
	return tab
}

// crée un nouveau tableau à partit de tab tel que les éléments du nouveau tableau sont composés des éléments du précédent tableau auxquels on a appliqué la fonction f
func MapP(tab []int, f func(int) int) []int {
	nbGoroutine := 4
	chunksize := len(tab) / nbGoroutine
	var wg sync.WaitGroup
	for i := 0; i < nbGoroutine; i++ {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			start := idx * chunksize
			end := start + chunksize
			if idx == nbGoroutine-1 {
				end = len(tab)
			}

			for j := start; j < end; j++ {
				tab[j] = f(tab[j])
			}
		}(i)
	}
	wg.Wait()
	return tab
}

// reduit le tableau à un entier (agrégation de tous les éléments avec la fonction f)
func ReduceN(tab []int, init int, f func(int, int) int) int {
	if len(tab) == 0 {
		return init
	} //very important to judge the end case and notice that init is updated every recursion
	return ReduceN(tab[1:], f(init, tab[0]), f)
}

// reduit le tableau à un entier (agrégation de tous les éléments avec la fonction f)
func ReduceP(tab []int, init int, f func(int, int) int) int {
	nbGoroutine := 4
	chunksize := len(tab) / nbGoroutine

	var wg sync.WaitGroup
	results := make(chan int, nbGoroutine)

	for i := 0; i < nbGoroutine; i++ {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			start := idx * chunksize
			end := start + chunksize
			if idx == nbGoroutine-1 {
				end = len(tab)
			}
			var res int = init
			for j := start; j < end; j++ {
				res = f(res, tab[j])
			}
			results <- res
		}(i)
	}
	wg.Wait()
	close(results)
	resFinal := 0
	for result := range results {
		resFinal = f(resFinal, result)
	}
	return resFinal
}

// vérifie que la condition f est vérifiée sur tous les éléments du tableau tab
func EveryN(tab []int, f func(int) bool) bool {
	for _, v := range tab {
		if !f(v) {
			return false
		}
	}
	return true
}

func EveryP(tab []int, f func(int) bool) bool {
	nbGoroutine := 4
	chunksize := len(tab) / nbGoroutine

	var wg sync.WaitGroup
	var once sync.Once
	resdone := make(chan bool)

	for i := 0; i < nbGoroutine; i++ {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			start := idx * chunksize
			end := start + chunksize
			if idx == nbGoroutine-1 {
				end = len(tab)
			}
			for j := start; j < end; j++ {
				select {
				case <-resdone:
					return
				default:
					if !f(tab[j]) {
						once.Do(func() { close(resdone) })
						return
					}
				}
			}
		}(i)
	}

	wg.Wait()
	select {
	case <-resdone:
		return false
	default:
		close(resdone)
		return true
	}
}

// vérifie que la condition f est vérifiée sur au moins un élément du tableau tab
func AnyN(tab []int, f func(int) bool) bool {
	for _, v := range tab {
		if f(v) {
			return true
		}
	}
	return false
}

func AnyP(tab []int, f func(int) bool) bool {
	nbGoroutine := 0
	chunksize := len(tab) / nbGoroutine

	var wg sync.WaitGroup
	var once sync.Once

	resDone := make(chan bool, 1)
	stopCh := make(chan bool)

	for i := 0; i < nbGoroutine; i++ {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			start := idx * chunksize
			end := start + chunksize
			if idx == nbGoroutine-1 {
				end = len(tab)
			}

			for j := start; j < end; j++ {
				select {
				case <-stopCh:
					return
				default:
					if f(tab[j]) {
						resDone <- true
						once.Do(func() { close(stopCh) })
						return
					}
				}
			}
		}(i)
	}

	wg.Wait()
	close(resDone)
	res, ok := <-resDone
	if ok && res {
		return true
	}
	return false
}

// tri le tableau tab
func SortN(tab []int, comp func(int, int) bool) {
	for i := 0; i < len(tab); i++ {
		for j := i; j < len(tab); j++ {
			if comp(tab[i], tab[j]) {
				tab[i], tab[j] = tab[j], tab[i]
			}
		}
	}
}

func Merge(left []int, right []int) []int {
	var res []int
	i := 0
	for ; len(left) > 0 && len(right) > 0; i++ {
		if left[0] <= right[0] {
			res[i] = left[0]
			left = left[1:]
		} else {
			res[i] = right[0]
			right = right[1:]
		}
	}

	for j := range left {
		res[i] = left[j]
		i++
	}
	for k := range right {
		res[i] = right[k]
		i++
	}
	//left = res[:len(left)]
	//right = res[len(left):]
	return res
}

// sort the array with merge sort in parallel (awsome for me T.T)
func SortP(tab []int, comp func(int, int) bool) {
	if len(tab) <= 1 {
		return
	}
	mid := len(tab) / 2
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		SortP(tab[:mid], comp)
	}()
	go func() {
		defer wg.Done()
		SortP(tab[mid:], comp)
	}()
	wg.Wait()
	tab = Merge(tab[:mid], tab[mid:])
}

func main() {
	start := time.Now()
	var tab [1000000000]int
	var arr []int = tab[:]
	FillP(arr, 3)
	fmt.Println("how many time takes: ", time.Since(start))
}
