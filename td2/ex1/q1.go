package main

import (
	"fmt"
	"time"
)

func compte(n int) {
	for i := 0; i < n+1; i++ {
		fmt.Println(i)
	}
}

func compteMsg(n int, msg string) {
	for i := 0; i < n+1; i++ {
		fmt.Println(msg, i)
	}
}

func compteMsgFromTo(start int, end int, msg string) {
	for i := start; i < end+1; i++ {
		fmt.Println(msg, i)
	}
}

func main() {
	for i := 0; i < 10; i++ {
		debut := i * 10
		fin := debut + 10
		go compteMsgFromTo(debut, fin, "good as heaven")
	}

	time.Sleep(time.Second)
	//fmt.Scanln()
}
