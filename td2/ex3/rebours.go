package main

import (
	"fmt"
	"time"
)

func countdownSleep() {
	for i := 5; i > 0; i-- {
		fmt.Println(i)
		time.Sleep(time.Second)
	}
	fmt.Println("Bonne année !")
}

func countdownAfter() {
	for i := 5; i > 0; i-- {
		fmt.Println(i)
		c := time.After(time.Second)
		fmt.Println(<-c)
	}

	fmt.Println("Bonne année !")
}

func countdownTick() {
	ticker := time.Tick(time.Second)
	for i := 5; i > 0; i-- {
		fmt.Println(i)
		<-ticker
	}
	fmt.Println("Bonne année !")
}

func main() {
	countdownTick()
}
