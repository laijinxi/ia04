package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"
)

var dict []string = []string{"AGENT", "CHIEN", "COLOC", "ETANG", "ELLE", "GEANT", "NICHE", "RADAR"}

func IsPalindrome(word string) bool {
	for i := 0; i < len(word); i++ {
		if i == len(word)-i-1 {
			break
		}

		if word[i] == word[len(word)-i-1] {
			continue
		} else {
			return false
		}
	}
	return true
}

func Palindromes(dict []string) []string {
	var palindromes []string
	for i := range dict {
		if IsPalindrome(dict[i]) {
			palindromes = append(palindromes, dict[i])
		}
	}
	return palindromes
}

// func Anagrammes(dict []string) []string
func Footprint(s string) (Footprint string) {
	var s1 []string = strings.Split(s, "")
	sort.Strings(s1)
	return strings.Join(s1, "")
}

func Anagrams(dict []string) (anagrams map[string][]string) {
	anagrams = make(map[string][]string)
	for _, str := range dict {
		anagrams[Footprint(str)] = append(anagrams[Footprint(str)], str)
	}
	return anagrams
}

// DictFromFile lit un ficher contenant un mot par ligne
// Renvoie une liste des mots contenus dans le ficher
func DictFromFile(filename string) (dict []string, err error) {
	file, err := os.Open(filename)
	if err != nil {
		fmt.Println("Error:", err)
		return nil, err
	}
	defer file.Close()

	var tmpDict []string
	filescanner := bufio.NewScanner(file)
	for filescanner.Scan() {
		tmpDict = append(tmpDict, filescanner.Text())
	}

	if err := filescanner.Err(); err != nil {
		return nil, err
	}
	return tmpDict, nil
}

func main() {
	filePath := "d:\\Cours\\IA04\\td1\\Problem\\dico-scrabble-fr.txt"
	newDict, err := DictFromFile(filePath)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	// for _, word := range newDict {
	// 	fmt.Println(word)
	// }

	//Question1!
	paDict := Palindromes(newDict)
	maxLongeur := 0
	var longestPalinDict []string
	for _, p := range paDict {
		if maxLongeur < len(p) {
			maxLongeur = len(p)
			longestPalinDict = append(longestPalinDict, p)
		} else if maxLongeur == len(p) {
			longestPalinDict = append(longestPalinDict, p)
		}
	}
	fmt.Println("Longest palindromes words:", longestPalinDict)
	//Question2
	anDict := Anagrams(newDict)
	key := Footprint("agent")
	fmt.Println(anDict[key])

	//Question3
	var longestAna []string
	maxAna := 0
	for key, val := range anDict {
		if maxAna < len(val) { // not len(key)
			maxAna = len(val)
			longestAna = []string{key}
		} else if maxAna == len(val) {
			longestAna = append(longestAna, key)
		}
	}

	//Question4
	for _, word := range paDict {
		if len(anDict[Footprint(word)]) > 1 {
			fmt.Println(word)
			break
		}
	}
	return
}
