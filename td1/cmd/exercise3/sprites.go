package main

import (
	"fmt"
	"math"
)

type Point2D struct {
	x float64
	y float64
}

func (p Point2D) X() float64 {
	return p.x
}

func (p Point2D) Y() float64 {
	return p.y
}

func (p *Point2D) SetX(val float64) {
	p.x = val
}

func (p *Point2D) SetY(val float64) {
	p.y = val
}

func NewPoint2D(x float64, y float64) Point2D {
	return Point2D{x, y}
}

func (p Point2D) Clone() Point2D {
	return Point2D{p.x, p.y}
}

func (p Point2D) Module() float64 {
	return math.Sqrt(p.x*p.x + p.y*p.y)
}

type Rectangle struct {
	topLeft     Point2D
	bottomRight Point2D
}

func NewRectanglePar2Points(HG Point2D, BD Point2D) Rectangle {
	return Rectangle{HG, BD}
}

func NewRectanglePar4Points(x1 float64, y1 float64, x2 float64, y2 float64) Rectangle {
	return Rectangle{Point2D{x1, y1}, Point2D{x2, y2}}
}

type Sprite struct {
	zoom      float64
	position  Point2D
	hitbox    Rectangle
	nomBitmap string
}

func NewSprite(pos Point2D, hbox Rectangle, nomfile string) Sprite {
	return Sprite{1, pos, hbox, nomfile}
}

func (s *Sprite) SetScaleFactor(f float64) {
	s.zoom = f
}

func (s *Sprite) Move(des Point2D) {
	s.position = des
}

func (s Sprite) Collision(other Sprite) bool {
	// Récupérer les rectangles pour les sprites en tenant compte du zoom
	rect1 := s.hitbox
	rect1.topLeft.SetX(s.position.X() + s.hitbox.topLeft.X()*s.zoom)
	rect1.topLeft.SetY(s.position.Y() + s.hitbox.topLeft.Y()*s.zoom)
	rect1.bottomRight.SetX(s.position.X() + s.hitbox.bottomRight.X()*s.zoom)
	rect1.bottomRight.SetY(s.position.Y() + s.hitbox.bottomRight.Y()*s.zoom)

	rect2 := other.hitbox
	rect2.topLeft.SetX(other.position.X() + other.hitbox.topLeft.X()*other.zoom)
	rect2.topLeft.SetY(other.position.Y() + other.hitbox.topLeft.Y()*other.zoom)
	rect2.bottomRight.SetX(other.position.X() + other.hitbox.bottomRight.X()*other.zoom)
	rect2.bottomRight.SetY(other.position.Y() + other.hitbox.bottomRight.Y()*other.zoom)

	// Vérifier la collision
	if rect1.bottomRight.X() < rect2.topLeft.X() ||
		rect1.topLeft.X() > rect2.bottomRight.X() ||
		rect1.bottomRight.Y() < rect2.topLeft.Y() ||
		rect1.topLeft.Y() > rect2.bottomRight.Y() {
		return false
	}
	return true
}

func main() {
	fmt.Println("Venus coming soon...")
}
