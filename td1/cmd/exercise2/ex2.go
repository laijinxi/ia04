package main

import (
	"fmt"
	"math/rand"
)

func Fill(s1 []int) {
	for i := 0; i < len(s1); i++ {
		s1[i] = rand.Intn(100)
	}
}

func Moyenne(s1 []int) float64 {
	var sum int
	for i := 0; i < len(s1); i++ {
		sum += s1[i]
	}
	return float64(sum) / float64(len(s1))
}

func ValeursCentrales(s1 []int) []int {
	if s1 == nil {
		return nil
	}
	if len(s1)%2 == 0 {
		return []int{s1[len(s1)/2-1], s1[len(s1)/2]}
	}
	return []int{s1[len(s1)/2]}
}

func Plus1(s1 []int) {
	for i := 0; i < len(s1); i++ {
		s1[i]++
	}
}

func Compte(tab []int) {
	for i := 0; i < len(tab); i++ {
		fmt.Printf("%v", tab[i])
	}
}

func main() {
	var s1 []int = make([]int, 10)
	Fill(s1)
	fmt.Println(s1)
	fmt.Println(Moyenne(s1))
	Plus1(s1)
	fmt.Println(Moyenne(s1))
	fmt.Println(ValeursCentrales(s1))
}
