package main

import "fmt"

func SayMeGoodbye(c chan string) {
	var s string
	for {
		s = <-c
		fmt.Printf("You said me: %q\n", s)
		if s == "Goodbye..." {
			c <- "bye"
			return
		} else {
			c <- "I don't understand"
		}
	}
}

type Rectangle struct {
	x float64
	y float64
	h float64
	w float64
}

func (r *Rectangle) Area() float64 {
	return r.h * r.w
}

// func (r *Rectangle) Translate(x float64, y float64) {
// 	r.x = r.x + x
// 	r.y = r.y + y
// }

func (r Rectangle) Translate(x float64, y float64) {
	r.x = r.x + x
	r.y = r.y + y
}

var r1 Rectangle

type Translatable interface {
	Translate(x float64, y float64)
}

func moveUp(r Translatable) {
	r.Translate(100, 0)
}

func main() {
	fmt.Println("Hello, World!")
}

// // List represents a singly-linked list that holds
// // values of any type.
// type List[T any] struct {
// 	next *List[T]
// 	val  T
// }

// // NewList creates a new list with a given value
// func NewList[T any](val T) *List[T] {
// 	return &List[T]{val: val}
// }

// func (ls *List[T]) Print() {
// 	for ls != nil {
// 		fmt.Printf("[%v] ", ls.val)
// 		ls = ls.next
// 	}
// 	fmt.Println()
// }

// func (ls *List[T]) Insert(v T) {
// 	newVertex := &List[T]{val: v}
// 	if ls == nil {
// 		ls = newVertex
// 		return
// 	}
// 	for ls.next != nil {
// 		ls = ls.next
// 	}
// 	ls.next = newVertex
// }

// func (ls *List[T]) Find(v T) *List[T] {
// 	for ls != nil {
// 		if ls.val == v {
// 			return ls
// 		}
// 		ls = ls.next
// 	}
// 	return nil
// }

// func (ls *List[T]) Delete(v T) {
// 	if ls == nil {
// 		return
// 	}

// 	if ls.val == v {
// 		*ls = *ls.next
// 		return
// 	}

// 	prev := ls
// 	for prev.next != nil && prev.next.val != v {
// 		prev = prev.next
// 	}

// 	if prev.next != nil {
// 		prev.next = prev.next.next
// 	}
// }

// func main() {
// 	list := NewList[int](1)
// 	list.Insert(2)
// 	list.Insert(3)
// 	list.Insert(4)
// 	list.Print() // Output: [1] [2] [3] [4]

// 	node := list.Find(3)
// 	if node != nil {
// 		fmt.Println("Found:", node.val) // Output: Found: 3
// 	} else {
// 		fmt.Println("Not found")
// 	}

// 	list.Delete(3)
// 	list.Print() // Output: [1] [2] [4]

// 	list.Delete(1)
// 	list.Print() // Output: [2] [4]
// }
